# Instruction FROM : image parente de base ubuntu
FROM python:rc-slim-buster

# Metadatas, pour donner des infos sur l'auteur, la version, description
LABEL maintainer="Groupe4"
LABEL description="Image avec application Nextcloud intégrée"

# Variables d'environnement
ENV ANSIBLE_VERSION 2.9.17

# Installations des tools

   #INSTALLATION DES DEPENDANCES
RUN apt-get update 
RUN apt-get -y install gcc make cmake build-essential libssl-dev libffi-dev python-dev

   # ANSIBLE
RUN pip3 install --upgrade pip
RUN pip3 install cryptography
RUN pip3 install "ansible==${ANSIBLE_VERSION}"

RUN pip3 install ansible
 
   # DOCKER et DOCKER-COMPOSE
RUN ansible-galaxy collection install community.docker
RUN pip3 install docker
RUN pip3 install docker-compose
